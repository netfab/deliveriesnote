<?php
/*
 *  This file is part of Deliveries Note Module, a module for Dolibarr.
 *  Copyright (C) 2012-2018 Fabrice Delliaux <netbox253@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *	\file		pdf_eclipse.modules.php
 *	\ingroup	deliveriesnote
 *	\brief		File to generate deliveries note PDF from eclipse model
 *	\version	7.0.1
 */

require_once(dirname(__DIR__).'/modules_deliveriesnote.php');

require_once(DOL_DOCUMENT_ROOT.'/core/lib/pdf.lib.php');
require_once(DOL_DOCUMENT_ROOT.'/core/lib/functions.lib.php');

class pdf_eclipse extends deliveriesnote_model
{
	private $error = 'simple error';

	private $orders = array();
	private $details = array();
	private $checked_refs = array();

	private $pdf_format;
	private $pdf_creation_date;
	private $selected_date;
	private $maxLineY;
	private $outputfile;
	private $currency;

	private $emetteur;

	// ordersList, ordersDetails, checked_orders_references
	function __construct($o, $d, $c, $s) {
		global $mysoc;

		$this->orders = $o;
		$this->details = $d;
		$this->checked_refs = $c;
		$this->selected_date = $s;

		$this->page_width = 210;
		$this->page_height = 297;
		$this->pdf_format = array($this->page_width,$this->page_height);

		$this->pagenb = 0;

		$this->marge_gauche=5;
		$this->marge_droite=5;
		$this->marge_haute=5;
		$this->marge_basse=5;

		// table properties
		$this->tab_top = 35;
		$this->tab_header_height = 10;
		$this->tab_width = $this->_getPageWidth() - $this->marge_gauche - $this->marge_droite;

		$this->lineHeight = 5;
		// minimal Table Line Coefficient
		$this->minTLC = 6;

		// table body start
		$this->_resetCurrentLineY();
		$this->maxLineY = $this->nextLineY + 30 * $this->lineHeight;

		$this->emetteur=$mysoc;
	}

	function __destruct() {
	}

	/*
	 *	Public Methods
	 *
	 */

	public function write_file($outfile, $outputlangs) {
		global $conf;

		$outputlangs->load('main');
		$outputlangs->load('orders');
		$outputlangs->load('deliveriesnote@deliveriesnote');

		if ( sizeof($this->orders) == 0 ) {
			$this->error = $outputlangs->transnoentities('DN_PDFWithoutOrders');
			return 0;
		}

		$this->outputfile = $outfile;
		$this->currency = $outputlangs->transnoentities('Currency'.$conf->currency);

		//print '<pre>';
		//print_r($this->orders);
		//print '</pre>';

		$this->default_font_size = pdf_getPDFFontSize($outputlangs);

		$this->pdf_creation_date = dol_print_date( dol_now(), $outputlangs->trans('FormatDateHourSecShort') );

		/*  PDF start */

		$pdf = $this->_initPDF($outputlangs);

		$this->_addPDFPage($pdf, $outputlangs);

		// draw table header and init columns.
		$this->_initTable($pdf, $outputlangs);

		$this->_fillTable($pdf, $outputlangs);

		$this->_closePDF($pdf);

		return 1;
	}

	/*
	 *	Protected Methods
	 *
	 */

	protected function _getPageWidth() {
		// landscape in _initPDF()
		return $this->page_height;
	}

	protected function _pageHead(&$pdf, $outputlangs) {
		global $conf;

		// TODO - TODO - TODO
		// pdf_pagehead
		// pdf_watermark

		$pdf->SetTextColor(0,0,60);
		$pdf->SetFont('','B', $this->default_font_size + 2);

		$pdf->SetXY($this->marge_gauche,$this->marge_haute);

		// Logo
		if( $this->emetteur->logo ) {
			$logo=$conf->mycompany->dir_output.'/logos/'.$this->emetteur->logo;
			if( is_readable($logo) )
				// width=0 (auto), max height=24
				$pdf->Image($logo, $this->marge_gauche, $this->marge_haute, 0, 24);
			else {
				$pdf->SetTextColor(200,0,0);
				$pdf->SetFont('','B',$this->default_font_size);
				$pdf->MultiCell(100, 3, $outputlangs->transnoentities("ErrorLogoFileNotFound",$logo), 0, 'L');
				$pdf->MultiCell(100, 3, $outputlangs->transnoentities("ErrorGoToGlobalSetup"), 0, 'L');
			}
		}
		else {
			$text=$this->emetteur->name;
			$pdf->MultiCell(100, 4, $outputlangs->convToOutputCharset($text), 0, 'L');
		}

		$pdf->SetFont('','B', $this->default_font_size);
		$pdf->SetXY(70,$this->marge_haute);
		$pdf->MultiCell(100, 4, $outputlangs->transnoentities('DN_CreationDate').' : ', 0, 'R');

		$pdf->SetFont('','', $this->default_font_size);
		$pdf->SetXY(170,$this->marge_haute);
		$pdf->MultiCell(100, 4, $outputlangs->convToOutputCharset($this->pdf_creation_date), 0, 'L');

		$pdf->SetFont('','B', $this->default_font_size);
		$pdf->SetXY(70,$this->marge_haute+4);
		$pdf->MultiCell(100, 4, $outputlangs->transnoentities('DN_SelectedDate').' : ', 0, 'R');

		$pdf->SetFont('','', $this->default_font_size);
		$pdf->SetXY(170,$this->marge_haute+4);
		if( is_null($this->selected_date) )
			$this->selected_date = $outputlangs->transnoentities('DN_None');
		$pdf->MultiCell(100, 4, $outputlangs->convToOutputCharset($this->selected_date), 0, 'L');

		$pdf->SetFont('','B', $this->default_font_size);
		$pdf->SetXY(70,$this->marge_haute+8);
		$pdf->MultiCell(100, 4, $outputlangs->transnoentities('DN_DeliveriesTotalNumber').' : ', 0, 'R');

		$pdf->SetFont('','', $this->default_font_size);
		$pdf->SetXY(170,$this->marge_haute+8);
		$pdf->MultiCell(100, 4, $outputlangs->convToOutputCharset(count($this->orders)), 0, 'L');

		$pdf->SetFont('','B', $this->default_font_size);
		$pdf->SetXY(70,$this->marge_haute+12);
		$pdf->MultiCell(100, 4, $outputlangs->transnoentities('DN_SelectedDeliveriesNumber').' : ', 0, 'R');

		$pdf->SetFont('','', $this->default_font_size);
		$pdf->SetXY(170,$this->marge_haute+12);
		$pdf->MultiCell(100, 4, $outputlangs->convToOutputCharset(count($this->checked_refs)), 0, 'L');
	}

	/*
	 *	Private Methods
	 *
	 */

	private function _initPDF($outputlangs) {
		global $user, $conf;

		// For backward compatibility with FPDF, force output charset to ISO,
		// because FPDF expect text to be encoded in ISO
		// if (!class_exists('TCPDF')) $outputlangs->charset_output='ISO-8859-1';

		// landscape
		$pdf = pdf_getInstance($this->pdf_format, 'mm', 'L');

		if (class_exists('TCPDF')) {
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
		}

		$pdf->SetFont(pdf_getPDFFont($outputlangs));

		$pdf->Open();

		$pdf->SetDrawColor(128,128,128);

		$subject = $outputlangs->transnoentities('Module451000Name');
		$author = $outputlangs->convToOutputCharset($user->getFullName($outputlangs));
		$keywords = $subject.' '.$outputlangs->convToOutputCharset($this->selected_date);

		$pdf->SetTitle($keywords);
		$pdf->SetSubject($subject);
		$pdf->SetCreator("Dolibarr ".DOL_VERSION);
		$pdf->SetAuthor($author);
		$pdf->SetKeyWords($keywords);
		if ($conf->global->MAIN_DISABLE_PDF_COMPRESSION)
			$pdf->SetCompression(false);

		// Left, Top, Right
		$pdf->SetMargins($this->marge_gauche, $this->marge_haute, $this->marge_droite);
		$pdf->SetAutoPageBreak(1,0);

		return $pdf;
	}

	private function _closePDF(&$pdf) {
		global $conf;

		// draw columns on last page and print page number
		$this->_drawColumns($pdf);
		$this->_drawPDFPageNo($pdf);

		$pdf->Close();

		$pdf->Output($this->outputfile,'F');
		if (! empty($conf->global->MAIN_UMASK))
			@chmod($file, octdec($conf->global->MAIN_UMASK));
	}

	private function _initTable(&$pdf, $outputlangs) {
		$this->_resetCurrentPosition();
		$this->tab_columns = array();

		$pdf->SetFillColor(230,230,230);
		// $pdf->SetDrawColor(128,128,128);

		/* $pdf->Rect( posX, posY, rectWidth, rectHeight) */
		$pX = $this->marge_gauche;
		$pY = $this->tab_top;
		$rW = $this->tab_width;
		$rH = $this->tab_header_height;
		$pdf->Rect($pX, $pY, $rW, $rH, 'FD');

		$t = $outputlangs->transnoentities('DN_OrderReference');
		$this->_initNextColumn($pdf, $t, 31);

		$t = $outputlangs->transnoentities('DN_OrderCustomer');
		$this->_initNextColumn($pdf, $t, 60);

		$t = $outputlangs->transnoentities('DN_OrderQuantity');
		$this->_initNextColumn($pdf, $t);

		$t = $outputlangs->transnoentities('DN_OrderDesignation');
		$this->_initNextColumn($pdf, $t, 50);

		$t = $outputlangs->transnoentities('DN_OrderTotalHT');
		$t .= ' ('.$this->currency.')';
		$this->_initNextColumn($pdf, $t, 30);

		$t = $outputlangs->transnoentities('DN_OrderVATRate');
		$this->_initNextColumn($pdf, $t);

		$t = $outputlangs->transnoentities('DN_DeliverySignature');
		$this->_initNextColumn($pdf, $t, 40);

		$t = $outputlangs->transnoentities('DN_OrderState');
		$this->_initNextColumn($pdf, $t, 35);

		// jump to the table body and set the font right after
		// table initialization, just before filling up the table.
		$this->_jumpToTableBody();

		$pdf->SetFont('','', $this->default_font_size);
	}

	private function _fillTable(&$pdf, $outputlangs) {
		$n = count($this->details);

		foreach($this->details as $key => $cmd) {

			$pass = true;
			foreach($this->checked_refs as $c_ref)
				if( $c_ref == $cmd['commande']->ref )
					$pass = false;

			if( $pass )
				continue;

			// order reference
			$t = $outputlangs->convToOutputCharset( $cmd['commande']->ref );
			$this->_fillTableCell($pdf, $t);

			// name address zip town phone
			$t  = $cmd['societe']->name;
			$t .= "\n".$cmd['societe']->address;
			$t .= "\n".$cmd['societe']->zip;
			$t .= ' '.$cmd['societe']->town;

			//$phone = dol_print_phone( $cmd['societe']->phone, $cmd['societe']->country_code, '', '', '', ' ' );
			$phone = chunk_split($cmd['societe']->phone, 2, ' ');
			$t .= "\n".$outputlangs->transnoentities('Phone').': '.$phone;
			$t = $outputlangs->convToOutputCharset($t);
			$this->_fillTableCell($pdf, $t);

			$quantities = ''; $labels = ''; $sums = ''; $rates= '';

			$this->curTLC = count($cmd['commande']->lines);

			foreach($cmd['commande']->lines as $orderkey => $orderline) {
				$quantities .= $orderline->qty."\n";
				$label = $orderline->product_label;
				// free line, unregistered product or service
				if( is_null( $label ) )
					$label = $orderline->desc;
				$labels .= $label."\n";
				$sums .= price($orderline->total_ht)."\n";
				$rates .= vatrate($orderline->tva_tx)."\n";
			}
			unset($label);

			// order quantities
			$quantities = $outputlangs->convToOutputCharset($quantities);
			$this->_fillTableCell($pdf, $quantities, 'C');

			// order labels
			$labels = $outputlangs->convToOutputCharset($labels);
			$this->_fillTableCell($pdf, $labels);

			// order prices
			$sums = $outputlangs->convToOutputCharset($sums);
			$this->_fillTableCell($pdf, $sums, 'R');

			// order TVA rates
			$rates = $outputlangs->convToOutputCharset($rates);
			$this->_fillTableCell($pdf, $rates, 'C');

			// skip signature column
			$this->_jumpToNextColumn();

			// order status
			//$status = $cmd['commande']->getLibStatut(0);
			$status = $this->_getOrderStatus($outputlangs, $cmd['commande']->statut, $cmd['commande']->facturee);
			$status = $outputlangs->convToOutputCharset($status);
			$this->_fillTableCell($pdf, $status, 'C');

			// --------
			// line end
			// --------
			$this->_drawNextHorizontalLine($pdf);
			$this->_jumpToNextTableLine();

			//print '<pre>';
			//print_r($cmd['commande']);
			//print '</pre>';

			// -------------------
			// check for next page
			// -------------------
			if($key < $n-1) {
				$max = count($this->details[$key+1]['commande']->lines);
				//print $key.' '.$n.' '.$max.'<br>';

				if( $this->_updateCurrentLineY($max) > $this->maxLineY ) {
					// draw columns of last page and print page number
					// before adding another page
					$this->_drawColumns($pdf);
					$this->_drawPDFPageNo($pdf);

					$this->_addPDFPage($pdf, $outputlangs);

					// draw table header and init columns
					$this->_initTable($pdf, $outputlangs);
				}
			}

		}

	}
}


?>
