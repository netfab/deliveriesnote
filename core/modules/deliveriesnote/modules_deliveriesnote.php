<?php
/*
 *  This file is part of Deliveries Note Module, a module for Dolibarr.
 *  Copyright (C) 2012-2018 Fabrice Delliaux <netbox253@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *	\file		htdocs/core/modules/deliveriesnote/modules_deliveriesnote.php
 *	\ingroup	deliveriesnote
 *	\brief		File defining parent class for deliveriesnote PDF models
 *	\version	7.0.1
 */

class ModelePDFDeliveriesnote
{
	static function liste_modeles($db,$maxfilenamelength=0)
	{
/*
		global $conf;

	$type='chequereceipt';
	$liste=array();

	include_once(DOL_DOCUMENT_ROOT.'/lib/functions2.lib.php');
	$liste=getListOfModels($db,$type,'');
*/
	// TODO Remove this to use getListOfModels only
	$liste = array('eclipse'=>'eclipse');

	return $liste;
	}
}





class deliveriesnote_model
{
	protected $default_font_size;

	protected $page_width;
	protected $page_height;

	protected $pagenb;

	protected $marge_gauche;
	protected $marge_droite;
	protected $marge_haute;
	protected $marge_basse;

	// table properties
	protected $tab_top;
	protected $tab_header_height;
	protected $tab_width;

	private $tab_columns = array();

	private $currentX;
	private $currentY;

	protected $lineHeight;
	protected $nextLineY;

	protected $curTLC;
	protected $minTLC;

	protected function _getPageWidth() {
		return $this->page_width;
	}

	protected function _jumpToNextTableLine() {
		$this->currentX = 0;
		$this->currentY++;
	}

	protected function _jumpToNextColumn() {
		//print $this->currentX.' '.' '.count($this->tab_columns).'<br>';

		// sanity check
		if( $this->currentX < count($this->tab_columns) )
			$this->currentX++;
	}

	protected function _resetCurrentPosition() {
		/*

        currentX        0           1               2               3

    currentY
                     -----------------------------------------------------------
         0          |   table   |   table   |               |
                    |   cell    |   cell    |     ...       |
                    |-----------------------------------------------------------
         1          |           |           |               |
                    |           |           |               |
                    |-----------------------------------------------------------
         2          |           |           |               |
                    |           |           |               |

		*/
		$this->currentY = -1;
		$this->_jumpToNextTableLine();
	}

	protected function _jumpToTableBody() {
		// back to table start ...
		$this->_resetCurrentPosition();
		// ... and skip table header.
		$this->_jumpToNextTableLine();
		$this->_resetCurrentLineY();
	}

	protected function _resetCurrentLineY() {
		// table body start
		$this->nextLineY = $this->tab_top + $this->tab_header_height;
	}

	protected function _updateCurrentLineY($coeff=-1) {
		if( $coeff == -1 )
			$coeff = $this->curTLC;
		if( $coeff < $this->minTLC )
			$coeff = $this->minTLC;
		$r = $this->nextLineY + $coeff * $this->lineHeight;
		return $r;
	}

	private function _getXPosition(&$pdf) {
		$sum = $this->marge_gauche;

		foreach($this->tab_columns as $key => $array) {
			if($key == $this->currentX)
				break;
			$s = 0;
			if( $array[1] == 0 ) {
				$s = $pdf->GetStringWidth($array[0]);
				$s = round( $s ) + 3;
			}
			else
				$s = $array[1];
			$sum += $s;
		}
		//print $sum.' '.$this->currentX.'<br />';
		return $sum;
	}

	private function _getColumnWidth(&$pdf) {
		$array = $this->tab_columns[$this->currentX];
		$s = $array[1];

		if( $s == 0 ) {
			$s = $pdf->GetStringWidth($array[0]);
			$s = round( $s ) + 4;
		}

		//if( $this->currentX == 0 )
		//	print $array[0].' '.$s.'<br />';
		return $s;
	}

	protected function _getOrderStatus($langs, $status, $facturee) {
		// commande/class/commande.class.php ->LibStatut()
		$s = 'unknown';
		switch( $status ) {
			case -1:
				$s = $langs->transnoentities('StatusOrderCanceled');
				break;
			case 0:
				$s = $langs->transnoentities('StatusOrderDraft');
				break;
			case 1:
				$s = $langs->transnoentities('StatusOrderValidated');
				break;
			case 2:
				$s = $langs->transnoentities('StatusOrderSentShort');
				break;
			case 3:
				if( $facturee )
					$s = $langs->transnoentities('StatusOrderProcessed');
				else
					$s = $langs->transnoentities('StatusOrderToBill');
				break;
		}
		return $s;
	}

	protected function _initNextColumn(&$pdf, $string, $minimal=0) {
		$pdf->SetFont('','B', $this->default_font_size);

		$this->tab_columns[] = array($string, $minimal);

		$this->_fillTableCell($pdf, $string, '', $this->tab_top);
	}

	protected function _drawColumns(&$pdf) {
		// the font size is used to calculate X position of columns
		// we're setting it up to the same as table header
		$pdf->SetFont('','B', $this->default_font_size);

		$this->_resetCurrentPosition();

		$n = count($this->tab_columns);

		foreach($this->tab_columns as $key => $value) {
			$this->_jumpToNextColumn();

			// don't draw the last one
			if($key == $n-1)
				break;

			$lineX = $this->_getXPosition($pdf)+1;
			$pdf->Line($lineX,$this->tab_top, $lineX, $this->nextLineY);
		}
		// first vertical line
		$lineX = $this->marge_gauche;
		$pdf->Line($lineX,$this->tab_top, $lineX, $this->nextLineY);
		// last vertical line
		$lineX += $this->tab_width;
		$pdf->Line($lineX,$this->tab_top, $lineX, $this->nextLineY);
	}

	protected function _fillTableCell(&$pdf, $string, $align='L', $vPos=-1) {
		// print $this->_getXPosition($pdf).'<br>';
		// print $this->_getColumnWidth($pdf).'<br>';
		$modif=0;
		if( $vPos == -1 ) {
			$vPos = $this->nextLineY;
			$modif=2;
		}
		$pdf->SetXY( $this->_getXPosition($pdf)+2+$modif, $vPos + 1 );
		$pdf->MultiCell($this->_getColumnWidth($pdf)-$modif, 2, $string, '', $align);
		$this->_jumpToNextColumn();
	}

	protected function _drawNextHorizontalLine(&$pdf) {
		$this->nextLineY = $this->_updateCurrentLineY();
		$pdf->Line($this->marge_gauche, $this->nextLineY, $this->marge_gauche+$this->tab_width, $this->nextLineY);
	}

	protected function _addPDFPage(&$pdf, $outputlangs) {
		// New page
		$pdf->AddPage();
		$this->pagenb++;

		$this->_pageHead($pdf, $outputlangs);
	}

	protected function _pageHead(&$pdf, $outputlangs) {
		// You must override it
	}


	protected function _drawPDFPageNo(&$pdf) {
		$pdf->SetFont('','', $this->default_font_size);

		$posy = $this->marge_basse + $this->lineHeight + 3;
		$pdf->SetXY(-25,-$posy);
		$pdf->MultiCell(18, 2, $pdf->PageNo().'/'.$pdf->getAliasNbPages(), 0, 'R', 0);

		//$pdf->AliasNbPages();
	}

}

?>
