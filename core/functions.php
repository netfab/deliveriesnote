<?php

/*
 *  This file is part of Deliveries Note Module, a module for Dolibarr.
 *  Copyright (C) 2012-2018 Fabrice Delliaux <netbox253@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

function redirect($extra) {
	$h = $_SERVER['HTTP_HOST'];
	$p = $_SERVER['PHP_SELF'];
	$uri = rtrim(dirname($p), '/\\');
	$full = "http://$h$uri/$extra";
	//print $full;
	header("Location: ".$full);
	exit;
}

?>
