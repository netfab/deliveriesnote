<?php
/*
 *  This file is part of Deliveries Note Module, a module for Dolibarr.
 *  Copyright (C) 2012-2018 Fabrice Delliaux <netbox253@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *	\file       htdocs/deliveriesnote/class/deliveriesnote.class.php
 *	\ingroup    deliveriesnote
 *	\brief      File of deliveriesnote class
 *	\version    7.0.1
 */

// TODO
// dol_syslog("add some debug messages", LOG_DEBUG);

require_once(DOL_DOCUMENT_ROOT.'/core/lib/functions.lib.php');
require_once(DOL_DOCUMENT_ROOT ."/commande/class/commande.class.php");
require_once(DOL_DOCUMENT_ROOT ."/societe/class/societe.class.php");

require_once(DOL_DOCUMENT_ROOT."/core/class/html.formfile.class.php");

class DeliveriesNote
{
	private $debug = 0;
	private $ordersList = array();
	private $ordersDetails = array();
	private $selectedDate = NULL;
	private $checked_refs = array();
	private $is_post = NULL;
	private $delivery_day = NULL;
	private $docPath = '';
	private $docSubDir = '';
	private $pdfFileFullPath = '';
	private $pdfFileName = '';
	private $allowUsage = 0;

	function __construct($month, $day, $year) {
		global $conf, $langs, $user;

		$this->allowUsage = $user->rights->deliveriesnote->use;

		$this->checked_refs = GETPOST('deliveries_ref', 2);
		if( $this->checked_refs == '' )
			$this->checked_refs = array();

		$this->is_post = GETPOST('NM_deliveries_post', 2);
		if( $this->is_post == '' )
			$this->is_post = NULL;

		$langs->load('deliveriesnote@deliveriesnote');

		// core/class/conf.class.php
		// 	function setValues
		//		// Define default dir_output and dir_temp for directories of modules
		$this->docPath = $conf->deliveriesnote->dir_output;
		$namePart = '';

		$this->delivery_day = $day;
		if( $this->delivery_day == '' ) {
			$this->delivery_day = NULL;
			$this->docSubDir = 'undefined';
		}
		else {
			$langs->load('orders');
			$dateformat = $langs->trans("FormatDateText");
			$timestamp = dol_mktime(1,0,0,$month,$day,$year,true);
			$this->selectedDate = dol_print_date($timestamp,$dateformat);

			$this->docSubDir = "$year/$month/$day";
			$namePart = '_'.$year.$month.$day;
		}

		dol_mkdir($this->docPath.DIRECTORY_SEPARATOR.$this->docSubDir);

		// Builds PDF file full path
		$pdfname = $langs->transnoentities('DN_PDFName');
		$pdfname.= $namePart;
		$num = 1;
		$tmpname = $pdfname.'_'.sprintf("%02d", $num);
		$tmpfile = $this->docPath.DIRECTORY_SEPARATOR.$this->docSubDir.DIRECTORY_SEPARATOR.$tmpname.'.pdf';

		while( file_exists($tmpfile) ) {
			$num++;
			if( $num > 5 )
				$num = 1;
			$tmpname = $pdfname.'_'.sprintf("%02d", $num);
			$tmpfile = $this->docPath.DIRECTORY_SEPARATOR.$this->docSubDir.DIRECTORY_SEPARATOR.$tmpname.'.pdf';
			if( $num == 1 )
				break;
		}

		//print $tmpfile;

		$this->pdfFileName = $tmpname.'.pdf';
		$this->pdfFileFullPath = $tmpfile;
	}

	function __destruct() {
	}

	/*
	 *	Public Methods
	 *
	 */

	public function checkDolibarrMinimalVersion( $vers ) {
		require_once(DOL_DOCUMENT_ROOT."/core/lib/admin.lib.php");

		$minimal_version = explode('.', $vers);
		$current_version = versiondolibarrarray();
		$ret = versioncompare( $current_version, $minimal_version );
		return ($ret >= 0) ? true : false;
	}

	public function pageHeader($help_url) {
		global $langs;

		$morejs = $this->allowUsage ? array("/deliveriesnote/js/deliveriesnote.js") : '';
		llxHeader('',$langs->trans("DN_Menu_DeliveriesNotes"),$help_url, '', '', '', $morejs);
	}

	public function storeOrder($order) {
		if( $this->allowUsage )
			$this->ordersList[] = $order;
	}

	public function showForm($action) {
		global $db, $conf;

		if( ! $this->allowUsage )
			return $this->showPermissionError();

		if( $action == 'builddoc' )
			$this->buildDocument();
		else if ($action == 'remove_file')
			$this->deleteDocument();

		$formfile = new Formfile($db);

		$this->showHeader();
		$this->showInformation();

		print '<table width="100%"><tr><td width="50%" valign="top">';
		print '<a name="builddoc"></a>'; // ancre

		/*
		 * Documents generes
		 *
		 */
		//$comref = dol_sanitizeFileName('livraisons.pdf');
		$comref = $this->docSubDir;
		//$file = $conf->deliveriesnote->dir_output . DIRECTORY_SEPARATOR . $comref . '.pdf';
		//$relativepath = $comref.'.pdf';
		$filedir = $this->docPath.DIRECTORY_SEPARATOR.$this->docSubDir;

		//$urlsource="/dolibarr/commande/liste.php?deliveryyear=$year&deliverymonth=$month&deliveryday=$day";
		$urlsource = $_SERVER['REQUEST_URI'];
		$a = explode( '&action=', $urlsource );
		$urlsource = $a[0];

		// FIXME
		$genallowed = $this->allowUsage;
		$delallowed = $this->allowUsage;

		$forname='builddoc';
		print PHP_EOL.'<form action="'.$urlsource.(empty($conf->global->MAIN_JUMP_TAG)?'':'#builddoc').'" name="'.$forname.'" id="'.$forname.'_form" method="post">'.PHP_EOL;
		print PHP_EOL.'<input type="hidden" value="aaa" name="NM_deliveries_post" />'.PHP_EOL;

		if( ! $this->debug )
			$css_invisibility = 'style="display:none;"';

		print PHP_EOL."<div $css_invisibility>".PHP_EOL;
		foreach ($this->ordersList as $objp) {
			$is_checked = $this->is_checked($objp->ref);
			print '<input type="checkbox" '.$is_checked.' value="'.$objp->ref.'" name="deliveries_ref[]" onclick="return readonlyCheckbox(this);" />'.PHP_EOL;
		}
		print '</div>'.PHP_EOL;

		//$somethingshown=$formfile->show_documents(  'commande',$comref,$filedir,$urlsource,$genallowed,$delallowed,
		//                                            $object->modelpdf,1,0,0,28,0,'','','',$soc->default_lang,$object->hooks);
		$somethingshown=$formfile->show_documents(
							'deliveriesnote',
							$comref,
							$filedir,
							$urlsource,
							$genallowed,
							$delallowed,
							'',				// $modelselected
							1,				// $allowgenifempty
							0,				// $forcenomultilang
							0,				// $iconPDF (deprecated)
							28,				// Not used
							1				// Do not output html form tags
							);

		print PHP_EOL.'</form>'.PHP_EOL;

		/*
		 * Linked object block
		 */
		//$somethingshown=$object->showLinkedObjectBlock();

		print '</td><td valign="top" width="50%">';

		// List of actions on element
		/*
		include_once(DOL_DOCUMENT_ROOT.'/core/class/html.formactions.class.php');
		$formactions=new FormActions($db);
		$somethingshown=$formactions->showactions($object,'order',$socid);
		*/
		print '</td></tr></table>';

		$this->showFooter();
	}

	public function showCheckbox($ref) {
		if( ! $this->allowUsage )
			return false;
		$is_checked = $this->is_checked($ref);
		print '<input type="checkbox" '.$is_checked.' value="'.$ref.'" name="del_ref[]" onclick="return changeCheckboxState(\'builddoc\', this);" />';
	}

	public function showDateLink($ddate, $date_delivery, $viewstatut) {
		global $db;

		if( ! $this->allowUsage ) {
			print $ddate;
			return;
		}

		$y = dol_print_date($db->jdate($date_delivery),'%Y');
		$m = dol_print_date($db->jdate($date_delivery),'%m');
		$d = dol_print_date($db->jdate($date_delivery),'%d');
		$myparams = 'viewstatut='.$viewstatut;
		$myparams.= '&amp;search_deliveryyear='.$y;
		$myparams.= '&amp;search_deliverymonth='.$m;
		$myparams.= '&amp;search_deliveryday='.$d;
		print ' <a href="'.$_SERVER['PHP_SELF'].'?'.$myparams.'">'.$ddate.'</a>';
	}


	/*
	 *	Private Methods
	 *
	 */

	private function deleteDocument() {
		global $langs;
		require_once(DOL_DOCUMENT_ROOT."/core/lib/files.lib.php");

		$langs->load("other");
		$filePath = GETPOST('file');
		$arr = explode( DIRECTORY_SEPARATOR, $filePath );
		$file = end( $arr );

		// disable glob and hooks when deleting file
		if( dol_delete_file($this->docPath.DIRECTORY_SEPARATOR.$filePath, 1, 0, 1) ) {
			//$mesg = '<div class="ok">'.$langs->trans("FileWasRemoved",GETPOST('file')).'</div>';
			$mesg = $langs->trans('FileWasRemoved',$file);
			dol_htmloutput_mesg($mesg);
		}
		else {
			$mesg = $langs->trans('DN_FileWasNotRemoved',$file);
			dol_htmloutput_errors($mesg);
		}
	}

	private function buildDocument() {
		global $db, $langs;

		$rvalue = $this->fetchDetails();

		if( $rvalue < 0 ) {
			dol_print_error(0, 'DeliveriesNote::buildDocument error while building document');
			return $rvalue;
		}

/*
    compta/facture.php #builddoc
    includes/modules/facture/modules_facture.php
*/

		$model = GETPOST('model');

		// sanity check
		if( gettype($this->checked_refs) !== 'array' ) {
			$error = $langs->transnoentities('DN_PDFWithoutOrders');
			dol_print_error($db, 'DeliveriesNote::buildDocument Error: '.$error);
			return -1;
		}

		$this->d_print($this->checked_refs);

		// Increase limit for PDF build
		$err=error_reporting();
		error_reporting(0);
		@set_time_limit(120);
		error_reporting($err);

		$dir = '/deliveriesnote/core/modules/deliveriesnote/';

		$file=''; $classname=''; $filefound=0;

		$file = 'pdf_'.$model.'.modules.php';

		$file = dol_buildpath($dir.'doc/'.$file);
	    
		if( file_exists($file) ) {
			$filefound=1;
			$classname='pdf_'.$model;
		}

		if ($filefound) {
			require_once($file);
			$obj = new $classname($this->ordersList, $this->ordersDetails, $this->checked_refs, $this->selectedDate);

			// FIXME
			// $conf->global->MAIN_MULTILANGS & $outputlangs

			// We save charset_output to restore it because write_file can change it if needed for
			// output format that does not support UTF8.
			$sav_charset_output=$langs->charset_output;

			$ret = $obj->write_file($this->pdfFileFullPath, $langs);

			// TODO check ret values
			switch( $ret ) {
				case -1:
					$langs->charset_output=$sav_charset_output;
					dol_print_error($db, 'DeliveriesNote::buildDocument Error: '.$obj->error);
					break;
				case 0:
					print '<p>'.img_warning( $obj->error ).' '.$obj->error.'</p>';
					dol_syslog( $obj->error, LOG_WARNING );
					break;
				case 1:
					// success
					$mesg = $langs->trans('DN_FileWasCreated',$this->pdfFileName);
					dol_htmloutput_mesg($mesg);
					break;
			}
			return $ret;
		}
		else {
			dol_print_error('',$langs->trans("Error")." ".$langs->trans("ErrorFileDoesNotExists",$file));
			return -1;
		}
	}

	private function d_print($o) {
		if( ! $this->debug )
			return;
		print '<pre>';
		print_r($o);
		print '</pre>';
	}

	private function showHeader() {
		global $langs;

		print PHP_EOL.PHP_EOL.'<!-- Begin Deliveries Note -->'.PHP_EOL.PHP_EOL;
		print '<br />';
		print_fiche_titre($langs->trans('DN_Menu_DeliveriesNotes'));
	}

	private function showFooter() {
		print PHP_EOL.PHP_EOL.'<!-- End Deliveries Note -->'.PHP_EOL.PHP_EOL;
	}

	private function showInformation() {
		global $langs;

		$text = $langs->transnoentities('DN_ClickOnDayForForm');

		$p = PHP_EOL.'<p>';
		$p.= PHP_EOL.img_info( $text ).' '.$text;
		$text = $langs->transnoentities('DN_CheckboxesInfo');
		$p.= PHP_EOL.'<br />'.img_info( $text ).' '.$text;
		$p.= PHP_EOL.'</p>';
		print $p;
	}

	private function showPermissionError() {
		global $langs;

		$this->showHeader();

		$text = $langs->trans('DN_PermissionError');
		$p = PHP_EOL.'<p>';
		$p.= PHP_EOL.img_error().' '.$text;
		$p.= PHP_EOL.'</p>';
		print $p;

		$this->showFooter();
		return false;
	}

	private function is_checked($value) {
		foreach($this->checked_refs as $c_ref)
			if( $c_ref == $value )
				return 'checked="checked"';
		// main list page
		if( is_null($this->delivery_day) )
			return '';
		// specific day
		else
			return (is_null($this->is_post)) ? 'checked="checked"' : '';
	}

	private function fetchDetails() {
		global $db;

		foreach($this->ordersList as $order) {
			$details = array(
				'commande' => 0,
				'societe'  => 0
			);

			$neworder = new Commande($db, $order->socid, $order->rowid);
			if ( $neworder->fetch($order->rowid) < 0 ) {
				dol_print_error(0, 'DeliveriesNote::fetchDetails error while fetching commande');
				return -1;
			}

			$details['commande'] = $neworder;

			$newsociete = new Societe($db, $order->socid);
			if ( $newsociete->fetch($order->socid) < 0 ) {
				dol_print_error(0, 'DeliveriesNote::fetchDetails error while fetching societe');
				return -1;
			}

			$details['societe'] = $newsociete;

			$this->ordersDetails[] = $details;

		}

		return 1;
	}


}

?>
