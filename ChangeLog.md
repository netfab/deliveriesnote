## ChangeLog of Consolidated deliveries Module

### version 7.0.1 - 2018-08-29
	* Maintenance update
	* Drop support for dolibarr < 8
	* Fix «bad value for modulepart» error with FormFile object
	  with dolibarr >= 8

### version 7.0.0 - 2018-06-26
	* Maintenance update - drop support for dolibarr < 7
	* Implement «read» right propertie to let non-admin users access
	  generated PDF documents
	* Update description and list files with newest ones coming from
	  dolibarr 7, and update code accordingly
	* Minor fixs

For previous versions and other changes, see the ChangeLog file into
the docs/ subdirectory of the module.
