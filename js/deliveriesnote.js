//
//  This file is part of DeliveriesNote Module, a module for Dolibarr.
//  Copyright (C) 2012-2018 Fabrice Delliaux <netbox253@gmail.com>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, version 3 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//	\file		deliveriesnote/js/deliveriesnote.js
//	\ingroup	DeliveriesNote
//	\brief		javascript
//	\version	7.0.1

function readonlyCheckbox(c){
	c.blur();
	return false;
}

function changeCheckboxState(f_name, c) {
	var F = window.document.forms[f_name];
	var checkboxes_array = F.elements["deliveries_ref[]"];
	var one_checked = false;
	// sanity check, should happen only on empty orders list
	if( checkboxes_array != undefined ) {
		var len = checkboxes_array.length;
		// one checkbox
		if(len == undefined) {
			if(c !== undefined)
				if(checkboxes_array.value == c.value)
					checkboxes_array.checked=(c.checked)?true:false;
			if(checkboxes_array.checked)
				one_checked = true;
		}
		// multiple checkboxes
		else if( len > 1 ) {
			for(var i=0; i<len; i++) {
				if(c !== undefined)
					if(checkboxes_array[i].value == c.value)
						checkboxes_array[i].checked=(c.checked)?true:false;
				if(checkboxes_array[i].checked)
					one_checked = true;
			}
		}
	}
	F.elements["builddoc_generatebutton"].disabled = !one_checked;
	if(c !== undefined)
		c.blur();
	return true;
}

function onLoadUpdates() {
	return changeCheckboxState('builddoc');
}

function addToOnLoad(o) {
	if( window.addEventListener )
		window.addEventListener('load', o, false);
	else if( window.attachEvent )
		window.attachEvent('onload', o);
	else
		document.addEventListener('load', o, false);
}

addToOnLoad( onLoadUpdates );

